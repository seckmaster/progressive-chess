package input;

import solver.Chessboard;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class InputGenerator {
  private FileParser fileParser;

  public InputGenerator(FileParser fileParser) {
    this.fileParser = fileParser;
  }

  public List<Chessboard> generateInputs(String locatedInDirectory) throws IOException {
    List<Chessboard> inputs = Files.walk(Paths.get(locatedInDirectory))
        .filter(Files::isRegularFile)
        .map(path -> parseFile(path.toString()))
        .filter(Optional::isPresent)
        .map(Optional::get)
        .map(fen -> Chessboard.fromFEN(fen))
        .collect(Collectors.toList());
    return inputs;
  }

  private Optional<String> parseFile(String path) {
    try {
      return Optional.ofNullable(fileParser.parseFile(path));
    } catch (IOException e) {
      return Optional.empty();
    }
  }
}
