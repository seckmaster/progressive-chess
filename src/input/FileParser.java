package input;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileParser {
  String parseFile(String file) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(file));
    String line = reader.readLine();
    reader.close();
    return line;
  }
}
