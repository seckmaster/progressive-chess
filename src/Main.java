import Rules.Move;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
public class Main {
  public static class FileParser {
    String parseFile(String file) throws IOException {
      BufferedReader reader = new BufferedReader(new FileReader(file));
      String line = reader.readLine();
      reader.close();
      return line;
    }
  }

  public static class InputGenerator {
    private FileParser fileParser;

    public InputGenerator(FileParser fileParser) {
      this.fileParser = fileParser;
    }

    public List<Chessboard> generateInputs(String locatedInDirectory) throws IOException {
      List<Chessboard> inputs = Files.walk(Paths.get(locatedInDirectory))
          .filter(Files::isRegularFile)
          .sorted(Comparator.comparingInt(o -> Integer.parseInt(o.getFileName().toString().split(".txt")[0])))
          .map(path -> parseFile(path.toString()))
          .filter(Optional::isPresent)
          .map(Optional::get)
          .map(fen -> Chessboard.fromFEN(fen))
          .collect(Collectors.toList());
      return inputs;
    }

    public Chessboard generateInput(String file) throws IOException {
      String fen = fileParser.parseFile(file);
      return Chessboard.fromFEN(fen);
    }

    private Optional<String> parseFile(String path) {
      try {
        return Optional.ofNullable(fileParser.parseFile(path));
      } catch (IOException e) {
        return Optional.empty();
      }
    }
  }

  public static class CoveringHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
    @Override
    public double heuristic(ChessNode node) {
      double covering = calculateCovering(
          Cache.movesForNode(node),
          node.getState().positionOfOpponentsKing());
      return covering;
    }

    private double calculateCovering(List<Move> availableMoves, Position opponentsKing) {
      int covering = availableMoves.stream()
          .mapToInt(move -> isMoveCovering(move, opponentsKing))
          .sum();
      return covering;
    }

    private int isMoveCovering(Move move, Position kingPosition) {
      int covering = 0;
      int[] coordinates = move.getCoordinates();
      for (int i = kingPosition.x - 1; i < kingPosition.x + 2; i++) {
        for (int j = kingPosition.y - 1; j < kingPosition.y + 2; j++) {
          if (i < 0 || i >= 8 || j < 0 || j >= 8) continue;
          if (coordinates[2] == i && coordinates[3] == j) covering += 1;;
        }
      }
      return covering;
    }
  }

  public static class DummyCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
    @Override
    public double heuristic(ChessNode node) {
      return 0;
    }
  }

  public static class KingMobilityHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
    @Override
    public double heuristic(ChessNode node) {
      Position opponentsKing = node.getState().positionOfOpponentsKing();
      return 7 - numberOfOccupiedMatingSquares(node.getState(), opponentsKing);
    }

    private int numberOfOccupiedMatingSquares(Chessboard chessboard, Position kingPosition) {
      int count = 0;
      for (int i = kingPosition.x - 1; i < kingPosition.x + 2; i++) {
        for (int j = kingPosition.y - 1; j < kingPosition.y + 2; j++) {
          if (i < 0 || i >= 8 || j < 0 || j >= 8) continue;
          if (chessboard.getBoard()[i][j] == Rules.Chessboard.EMPTY) ++count;
        }
      }
      return count;
    }
  }

  public interface ActionsGenerator<A, S, N extends Node<A, S>> {
    Iterable<A> possibleActionsForState(N node);
    NodeFactory<A, S, N> nodeFactory();
  }

  /**
   * Sum of manhattan distances between all pieces and opponent's king.
   */
  public static class ManhattanDistanceHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
    @Override
    public double heuristic(ChessNode node) {
      switch (node.getState().getColor()) {
        case Rules.Chessboard.WHITE:
          return sumDistancesFromWhitePiecesToBlackKing(node.getState());
        case Rules.Chessboard.BLACK:
          return sumDistancesFromBlackPiecesToWhiteKing(node.getState());
        default:
          return -1;
      }
    }

    private int sumDistancesFromWhitePiecesToBlackKing(Chessboard state) {
      return sumDistances(state, true);
    }

    private int sumDistancesFromBlackPiecesToWhiteKing(Chessboard state) {
      return sumDistances(state, false);
    }

    private int sumDistances(Chessboard state, boolean white) {
      int distances = 0;
      Position king = state.positionOfOpponentsKing();
      for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
          if (white) {
            if (state.getBoard()[i][j] > Rules.Chessboard.EMPTY)
              distances += manhattanDistance(king.x, king.y, i, j);
          } else if (state.getBoard()[i][j] < Rules.Chessboard.EMPTY) {
            distances += manhattanDistance(king.x, king.y, i, j);
          }
        }
      }
      return distances;
    }

    private int manhattanDistance(int x1, int y1, int x2, int y2) {
      return Math.abs(x1 - x2) + Math.abs(y1 - y2);
    }
  }

  public static class ProgressiveChessActionsGenerator implements ActionsGenerator<Move, Chessboard, ChessNode> {
    private NodeFactory factory = new ConcreteNodeFactory();

    @Override
    public Iterable<Move> possibleActionsForState(ChessNode node) {
      return Cache.movesForNode(node);
    }

    @Override
    public NodeFactory<Move, Chessboard, ChessNode> nodeFactory() {
      return factory;
    }
  }

  public static class Cache {
    private static HashMap<Long, List<Move>> movesCache = new HashMap<>();

    public static List<Move> movesForNode(ChessNode node) {
      Long hash = node.hashString();
      List<Move> cachedMoves = movesCache.get(hash);
      if (cachedMoves == null) {
        List<Move> moves = node.getState().getMoves();
        movesCache.put(hash, moves);
        return moves;
      }
      return cachedMoves;
    }

    public static void clear() {
      movesCache.clear();
    }
  }

  public interface Hashable<Result> {
    Result hashString();
  }

  public static class ZobristHasher {
    private long[][][] zobristTable = new long[8][8][12];
    private long[] movesLeftTable = new long[10];

    public ZobristHasher() {
      Random random = new Random();
      for (int i = 0; i<8; i++) {
        for (int j = 0; j < 8; j++) {
          for (int k = 0; k < 12; k++) {
            zobristTable[i][j][k] = Math.abs(random.nextLong());
          }
        }
      }
      for (int i = 0; i < movesLeftTable.length; i++) {
        movesLeftTable[i] = Math.abs(random.nextLong());
      }
    }

    public long hashPosition(int[][] board, int movesLeft) {
      long h = 0;
      for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
          int piece = board[i][j];
          if (piece == Chessboard.EMPTY) { continue; }
          if (piece == 0) continue;
          if (piece < Chessboard.EMPTY)
            piece = 12 + piece;
          else
            piece -= 1;
          h ^= zobristTable[i][j][piece];
        }
      }
      h ^= movesLeftTable[movesLeft];
      return h;
    }

    public long applyMove(long hash, Move move, int movedPiece, int movesLeft, int capturedPiece, int[][] board) {
      int modifiedPiece;
      if (movedPiece < Chessboard.EMPTY)
        modifiedPiece = 12 + movedPiece;
      else
        modifiedPiece = movedPiece - 1;
      int[] coordinates = move.getCoordinates();
//      long trueHash = singleton.hashPosition(board, movesLeft);
      long updatedHash = hash;
      if (capturedPiece != 0) {
        if (capturedPiece < Chessboard.EMPTY)
          capturedPiece = 12 + capturedPiece;
        else
          capturedPiece = capturedPiece - 1;
        updatedHash ^= zobristTable[coordinates[2]][coordinates[3]][capturedPiece];
      }
//      System.out.println(move);
      updatedHash ^= zobristTable[coordinates[0]][coordinates[1]][modifiedPiece];
      updatedHash ^= zobristTable[coordinates[2]][coordinates[3]][modifiedPiece];
      updatedHash ^= movesLeftTable[movesLeft];
      updatedHash ^= movesLeftTable[movesLeft + 1];
//      System.out.println(movesLeft);
//      if (trueHash != updatedHash) {
//        System.out.println();
//      }
      return updatedHash;
    }

    public static ZobristHasher singleton = new ZobristHasher(); // TODO: -
  }

  public static class BishopHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
    private HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator;
    private HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator;
    private HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator;

    public BishopHeuristicCalculator(HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator,
                                     HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator,
                                     HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator) {
      this.kingMobilityCalculator = kingMobilityCalculator;
      this.manhattanDistanceCalculator = manhattanDistanceCalculator;
      this.coveringCalculator = coveringCalculator;
    }

    @Override
    public double heuristic(ChessNode node) {
      double mobility = kingMobilityCalculator.heuristic(node);
      double manhattan = manhattanDistanceCalculator.heuristic(node);
      double covering = coveringCalculator.heuristic(node);
      return mobility + manhattan + covering;
    }
  }

  public static double
      PARAM_a = 7,
      PARAM_b = 8,
      PARAM_c = 16,
      PARAM_d = 3;

  public static class Result implements Comparable {
    double a, b, c, d;
    long time;
    int count;

    public Result(long time, int count) {
      this.time = time;
      this.count = count;
      this.a = PARAM_a;
      this.b = PARAM_b;
      this.c = PARAM_c;
      this.d = PARAM_d;
    }

    @Override
    public int compareTo(Object o) {
      return (int) (this.time - ((Result) o).time);
    }

    @Override
    public String toString() {
      return String.format("%d %d %.2f %.2f %.2f %.2f", time, count, a, b, c, d);
    }
  }

  public static List<Result> results = new ArrayList<>();

  public static class KingHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
    private HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator;
    private HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator;
    private HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator;

    public KingHeuristicCalculator(HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator,
                                   HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator,
                                   HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator) {
      this.kingMobilityCalculator = kingMobilityCalculator;
      this.manhattanDistanceCalculator = manhattanDistanceCalculator;
      this.coveringCalculator = coveringCalculator;
    }

    @Override
    public double heuristic(ChessNode node) {
      double defaultValue = Double.MAX_VALUE;
      double mobility = kingMobilityCalculator.heuristic(node);
      Position myKing = node.getState().positionOfMyKing();
      Position opponentsKing = node.getState().positionOfOpponentsKing();
      int distance = manhattanDistance(myKing.x, myKing.y, opponentsKing.x, opponentsKing.y);
      if (distance >= PARAM_a) {
        return defaultValue;
      }
      return distance * PARAM_b + mobility * PARAM_c;
    }

    private int manhattanDistance(int x1, int y1, int x2, int y2) {
      return Math.abs(x1 - x2) + Math.abs(y1 - y2);
    }
  }

  public static class KnightHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
    private HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator;
    private HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator;
    private HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator;

    public KnightHeuristicCalculator(HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator,
                                     HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator,
                                     HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator) {
      this.kingMobilityCalculator = kingMobilityCalculator;
      this.manhattanDistanceCalculator = manhattanDistanceCalculator;
      this.coveringCalculator = coveringCalculator;
    }

    @Override
    public double heuristic(ChessNode node) {
      double mobility = kingMobilityCalculator.heuristic(node);
      double manhattan = manhattanDistanceCalculator.heuristic(node);
      double covering = coveringCalculator.heuristic(node);
      return mobility + manhattan + covering;
    }
  }

  public static class PawnHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
    private HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator;
    private HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator;
    private HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator;

    public PawnHeuristicCalculator(HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator,
                                   HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator,
                                   HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator) {
      this.kingMobilityCalculator = kingMobilityCalculator;
      this.manhattanDistanceCalculator = manhattanDistanceCalculator;
      this.coveringCalculator = coveringCalculator;
    }

    @Override
    public double heuristic(ChessNode node) {
      double promotionDistance = distanceToPromotion(node.getState(), node.getAction().get());
      if (promotionDistance - 1 > node.getState().getMovesLeft()) {
        return Double.MAX_VALUE;
      }
      double reward = rewardForPromotion(node.getAction().get());
      double mobility = kingMobilityCalculator.heuristic(node);
      double manhattan = manhattanDistanceCalculator.heuristic(node);
      return promotionDistance * PARAM_d + manhattan + mobility + reward;
    }

    public int distanceToPromotion(Chessboard state, Move action) {
      switch (state.getColor()) {
        case Rules.Chessboard.WHITE:
          return 7 - action.getCoordinates()[2];
        case Rules.Chessboard.BLACK:
          return action.getCoordinates()[2];
        default:
          return -1;
      }
    }

    public double rewardForPromotion(Move action) {
      int piece = Math.abs(action.getPromotion());
      switch (piece) {
        case Rules.Chessboard.QUEEN:
          return 0;
        case Rules.Chessboard.ROOK:
          return 10;
        case Rules.Chessboard.KNIGHT:
        case Rules.Chessboard.BISHOP:
          return 15;
        default:
          return 0;
      }
    }
  }

  public static class QueenHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
    private HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator;
    private HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator;
    private HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator;

    public QueenHeuristicCalculator(HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator,
                                    HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator,
                                    HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator) {
      this.kingMobilityCalculator = kingMobilityCalculator;
      this.manhattanDistanceCalculator = manhattanDistanceCalculator;
      this.coveringCalculator = coveringCalculator;
    }

    @Override
    public double heuristic(ChessNode node) {
      double mobility = kingMobilityCalculator.heuristic(node);
      double manhattan = manhattanDistanceCalculator.heuristic(node);
      double covering = coveringCalculator.heuristic(node);
      return mobility + manhattan + covering;
    }
  }

  public static class RookHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
    private HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator;
    private HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator;
    private HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator;

    public RookHeuristicCalculator(HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator,
                                   HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator,
                                   HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator) {
      this.kingMobilityCalculator = kingMobilityCalculator;
      this.manhattanDistanceCalculator = manhattanDistanceCalculator;
      this.coveringCalculator = coveringCalculator;
    }

    @Override
    public double heuristic(ChessNode node) {
      double mobility = kingMobilityCalculator.heuristic(node);
      double manhattan = manhattanDistanceCalculator.heuristic(node);
      double covering = coveringCalculator.heuristic(node);
      return mobility + manhattan + covering;
    }
  }

  public static class ChessHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
    private int solutionSize;
    private Map<Integer, HeuristicCalculator> calculators;
    private HeuristicCalculator<Move, Chessboard, ChessNode> manhattanCalculator;

    public ChessHeuristicCalculator(int solutionSize,
                                    Map<Integer, HeuristicCalculator> calculatorMap,
                                    HeuristicCalculator<Move, Chessboard, ChessNode> manhattanCalculator) {
      this.solutionSize = solutionSize;
      this.calculators = calculatorMap;
      this.manhattanCalculator = manhattanCalculator;
    }

    public void registerCalculatorForPiece(HeuristicCalculator calculator, int piece) {
      calculators.put(Math.abs(piece), calculator);
    }

    @Override
    public double heuristic(ChessNode node) {
      if (!node.getAction().isPresent()) {
        // heuristic for the initial state
        // TODO: - figure out which heuristic, or a group of them, suits best
        return 0;
      }
      if (node.getAction().get().getPromotion() != 0) {
        return heuristicForPiece(Rules.Chessboard.PAWN, node);
      }
      int piece = node.getState().pieceAt(node.getAction().get());
      return heuristicForPiece(piece, node);
    }

    private double heuristicForPiece(int piece, ChessNode node) {
      HeuristicCalculator calculator = calculators.get(Math.abs(piece));
      if (calculator == null) {
        return manhattanCalculator.heuristic(node);
      }
      return calculator.heuristic(node);
    }
  }

  public interface HeuristicCalculator<A, S, N extends Node<A, S>> {
    double heuristic(N node);
  }

  public static class ChessNode extends Node<Move, Chessboard> {
    public final int depth;
    private Optional<Long> cachedHash = Optional.empty();

    public ChessNode(Chessboard newState, Move action, ChessNode parent) {
      super(newState, action, parent);
      this.depth = parent.depth + 1;
    }

    public ChessNode(Chessboard state) {
      super(state);
      depth = 0;
    }

    @Override
    public boolean isGoalReached() {
      return state.isCheckmate();
    }

    @Override
    public Long hashString() {
      if (cachedHash.isPresent()) {
        return cachedHash.get();
      }
//      int[][] board = getState().getBoard();
//      long hash = ZobristHasher.singleton.hashPosition(board, getState().getMovesLeft());
//      cachedHash = Optional.of(hash);
//      return hash;
      if (!parent.isPresent()) {
        long hash = ZobristHasher.singleton.hashPosition(
            state.getBoard(),
            getState().getMovesLeft()
        );
        cachedHash = Optional.of(hash);
        return hash;
      }

      ChessNode parentNode = (ChessNode) parent.get();

      long parentHash = parentNode.hashString();
      long hash = ZobristHasher.singleton.applyMove(
          parentHash,
          action.get(),
          state.pieceAt(action.get()),
          state.getMovesLeft(),
          parentNode.state.pieceAt(action.get()),
          state.getBoard()
      );
      cachedHash = Optional.of(hash);
      return hash;
    }
  }

  public static class ConcreteNodeFactory implements NodeFactory<Move, Chessboard, ChessNode> {
    @Override
    public ChessNode node(Move withAction, ChessNode parent) {
      Chessboard newState = parent.getState().copy(); // TODO: - Optimize this!!!!
      newState.makeMove(withAction);
      ChessNode node = new ChessNode(newState, withAction, parent);
      return node;
    }

    @Override
    public ChessNode node(Chessboard initialState) {
      ChessNode node = new ChessNode(initialState);
      return node;
    }
  }

  public static abstract class Node<A, S> implements Hashable<Long> {
    protected double g = Double.MAX_VALUE;
    protected double h = Double.MAX_VALUE;
    protected S state;
    protected Optional<A> action;
    protected Optional<Node> parent;

    public Node(S newState, A action, Node<A, S> parent) {
      this.state = newState;
      this.action = Optional.of(action);
      this.parent = Optional.of(parent);
    }

    public Node(S state) {
      this.state = state;
      this.action = Optional.empty();
      this.parent= Optional.empty();
    }

    public abstract boolean isGoalReached();

    public void setG(double g) {
      this.g = g;
    }

    public void setH(double h) {
      this.h = h;
    }

    public double getG() {
      return g;
    }

    public double getH() {
      return h;
    }

    public double getScore() {
      return g + h;
    }

    public S getState() {
      return state;
    }

    public Optional<A> getAction() {
      return action;
    }

    public Optional<Node> getParent() {
      return parent;
    }
  }

  public interface NodeFactory<A, S, N> {
    N node(A withAction, N parent);
    N node(S fromState);
  }

  public interface Algorithm<A, S, N extends Node<A, S>> {
    List<N> solve(S initialState);
  }

  public static class AStarAlgorithm<A, S, N extends Node<A, S>> implements Algorithm<A, S, N> {
    // Provider of actions for each state
    private ActionsGenerator<A, S, N> actionsGenerator;
    // Heuristics provider
    private HeuristicCalculator<A, S, N> heuristicCalculator;
    // The set of nodes already evaluated
    private Map<Long, N> closed = new HashMap<>();
    // The set of currently discovered nodes that are not evaluated yet.
    // Initially, only the start node is known.
    private Map<Long, N> open = new HashMap<>();
    private Queue<N> queue;

    private long startTime = System.currentTimeMillis();

    public AStarAlgorithm(ActionsGenerator<A, S, N> actionsGenerator, HeuristicCalculator<A, S, N> heuristicCalculator) {
      this.actionsGenerator = actionsGenerator;
      this.heuristicCalculator = heuristicCalculator;
      this.queue = new PriorityQueue<>(Comparator.comparingDouble(Node::getScore));
    }

    @Override
    public List<N> solve(S initialState) {
      N startingNode = actionsGenerator.nodeFactory().node(initialState);
      // The cost of going from start to start is zero.
      startingNode.setG(0);

      // For the first node, that value is completely heuristic.
      startingNode.setH(heuristicCalculator.heuristic(startingNode));

      // The set of currently discovered nodes that are not evaluated yet.
      // Initially, only the start node is known.
      queue.add(startingNode);
      open.put(startingNode.hashString(), startingNode);

      // TODO: - hacking (:
      Chessboard chessboard = (Chessboard) initialState;
      int numberOfMoves = chessboard.getMovesLeft();
      //

      while (!queue.isEmpty()) {
//        if (System.currentTimeMillis() - startTime >= 60000) { break; }

        N current = promisingNode();
        open.remove(current.hashString());
        closed.put(current.hashString(), startingNode);
        if (current.isGoalReached()) {
          // done searching
          return reconstructPath(current);
        }

        Iterable<A> actions = actionsGenerator.possibleActionsForState(current);
        for (A action: actions) {
//          if (System.currentTimeMillis() - startTime >= 60000) { break; }

          N neighbour = actionsGenerator.nodeFactory().node(action, current);
          if (closed.containsKey(neighbour.hashString())) {
            // Ignore the neighbor which is already evaluated.
            continue;
          }

          // The distance from start to a neighbor
          int distance = numberOfMoves - ((ChessNode) neighbour).depth;
          double tentativeScore = current.getG() + distance;

          N found = open.get(neighbour.hashString());
          if (found == null) {
            updateNode(neighbour, tentativeScore);
            open.put(neighbour.hashString(), neighbour);
            queue.add(neighbour);
          } else {
            if (tentativeScore >= neighbour.getG()) {
              continue;
            } else {
              updateNode(neighbour, tentativeScore);
            }
          }
        }
      }
      return Collections.emptyList();
    }

    private void updateNode(N node, double score) {
      node.setG(score);
      node.setH(heuristicCalculator.heuristic(node));
    }

    private N promisingNode() {
      return queue.poll();
    }

    private List<N> reconstructPath(N lastNode) {
      LinkedList<N> path = new LinkedList<>();
      path.add(lastNode);
      Optional parent = lastNode.getParent();
      while (parent.isPresent()) {
        N node = (N) parent.get();
        if (!node.getAction().isPresent()) { break; }
        path.add(0, node);
        parent = node.getParent();
      }
      return path;
    }
  }

  public static class Chessboard extends Rules.Chessboard {
    private Optional<Position> opponentsKing = Optional.empty();
    private Optional<Position> myKing = Optional.empty();

    protected Chessboard(int[][] board, int gameStatus, int color, int movesLeft) {
      super(board, gameStatus, color, movesLeft);
    }

    public static Chessboard fromFEN(String fen) {
      Rules.Chessboard chessboard = Rules.Chessboard.getChessboardFromFEN(fen);
      return new Chessboard(
          chessboard.getBoard(),
          chessboard.getGameStatus(),
          chessboard.getColor(),
          chessboard.getMovesLeft());
    }

    /////

    public Position positionOfOpponentsKing() {
      if (opponentsKing.isPresent()) {
        return opponentsKing.get();
      }

      Position position;
      switch (getColor()) {
        case Rules.Chessboard.WHITE:
          position = positionOfBlackKing();
          break;
        case Rules.Chessboard.BLACK:
          position = positionOfWhiteKing();
          break;
        default:
          position = null;
      }
      opponentsKing = Optional.of(position);
      return position;
    }

    public Position positionOfMyKing() {
      if (myKing.isPresent()) {
        return myKing.get();
      }

      Position position;
      switch (getColor()) {
        case Rules.Chessboard.WHITE:
          position = positionOfWhiteKing();
          break;
        case Rules.Chessboard.BLACK:
          position = positionOfBlackKing();
          break;
        default:
          position = null;
      }
      myKing = Optional.of(position);
      return position;
    }

    public int pieceAt(Move move) {
      int[] coordinates = move.getCoordinates();
      int piece = board[coordinates[2]][coordinates[3]];
      return piece;
    }

    public Position positionOfWhiteKing() {
      return positionOf(Rules.Chessboard.KING);
    }

    public Position positionOfBlackKing() {
      return positionOf(Rules.Chessboard.KING_B);
    }

    public Position positionOf(int piece) {
      for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
          if (board[i][j] == piece)
            return new Position(i, j);
        }
      }
      return null;
    }

    public boolean isCheckmate() {
      return gameStatus == 2;
    }

    @Override
    public void makeMove(Move move) {
      makeMove(move, false);
    }

    @Override
    public Chessboard copy() {
      int[][] nv = new int[this.board.length][this.board[0].length];

      for(int i = 0; i < nv.length; ++i) {
        nv[i] = Arrays.copyOf(this.board[i], this.board[i].length);
      }

      Chessboard b = new Chessboard(nv, this.gameStatus, this.color, this.movesLeft);
      b.a1 = this.a1;
      b.a8 = this.a8;
      b.e1 = this.e1;
      b.e8 = this.e8;
      b.h1 = this.h1;
      b.h8 = this.h8;
      b.movesCalculated = false;
      return b;
    }
  }

  public static class Position {
    public final int x, y;

    public Position(int x, int y) {
      this.x = x;
      this.y = y;
    }
  }

  public static void main(String[] args) throws IOException {
    if (args.length < 1) { return; }
    int mode = 0;
    if (args.length == 2) {
      mode = Integer.parseInt(args[1]);
    }
    List<Chessboard> chessboards = null;
    PrintStream outputStream = null;
    String fen;
    Chessboard chessboard = null;

    switch (mode) {
      case 0:
        String file = args[0];
        chessboard = new InputGenerator(new FileParser()).generateInput(file);
        break;
      case 1:
        fen = args[0];
        chessboard = Chessboard.fromFEN(fen);
        chessboards = new LinkedList<>();
        chessboards.add(chessboard);
        outputStream = System.out;
        break;
      case 2:
        int count = 1;
        try {
          BufferedReader reader = new BufferedReader(new FileReader("out/.counter"));
          count = Integer.parseInt(reader.readLine()) + 1;
          reader.close();
        } catch (IOException e) {}
        try {
          BufferedWriter writer = new BufferedWriter(new FileWriter("out/.counter"));
          writer.write(String.valueOf(count));
          writer.close();
        } catch (IOException e) {}
        InputGenerator generator = new InputGenerator(new FileParser());
        chessboards = generator.generateInputs("progressive_checkmates");
        outputStream = new PrintStream(new FileOutputStream("out/output["+count+"].txt"));
        outputStream.println(lastCommitSummary());
        break;
      default:
        System.err.println("Invalid mode");
        System.exit(2);
        return;
    }

    ChessHeuristicCalculator calculator = prepareHeuristics();
    ActionsGenerator actionsGenerator = new ProgressiveChessActionsGenerator();
    if (mode == 0) {
      solvePosition(chessboard, actionsGenerator, calculator);
    } else {
      solvePositions(chessboards, actionsGenerator, calculator, outputStream);
    }
  }

  private static ChessHeuristicCalculator prepareHeuristics() {
    HeuristicCalculator dummy = new DummyCalculator();
    HeuristicCalculator mobility = new KingMobilityHeuristicCalculator();
    HeuristicCalculator manhattan = new ManhattanDistanceHeuristicCalculator();
    HeuristicCalculator covering = new CoveringHeuristicCalculator();

    HeuristicCalculator pawn = new PawnHeuristicCalculator(mobility, manhattan, dummy);
    HeuristicCalculator bishop = new BishopHeuristicCalculator(dummy, manhattan, dummy);
    HeuristicCalculator king = new KingHeuristicCalculator(mobility, manhattan, dummy);
    HeuristicCalculator knight = new KnightHeuristicCalculator(dummy, manhattan, dummy);
    HeuristicCalculator queen = new QueenHeuristicCalculator(mobility, manhattan, dummy);
    HeuristicCalculator rook = new RookHeuristicCalculator(dummy, manhattan, dummy);

    ChessHeuristicCalculator calculator = new ChessHeuristicCalculator(0, new HashMap<>(), manhattan);
    calculator.registerCalculatorForPiece(pawn, Rules.Chessboard.PAWN);
    calculator.registerCalculatorForPiece(bishop, Rules.Chessboard.BISHOP);
    calculator.registerCalculatorForPiece(king, Rules.Chessboard.KING);
    calculator.registerCalculatorForPiece(knight, Rules.Chessboard.KNIGHT);
    calculator.registerCalculatorForPiece(queen, Rules.Chessboard.QUEEN);
    calculator.registerCalculatorForPiece(rook, Rules.Chessboard.ROOK);
    return calculator;
  }

  private static void solvePositions(List<Chessboard> positions,
                                     ActionsGenerator actionsGenerator,
                                     ChessHeuristicCalculator heuristicCalculator,
                                     PrintStream outputStream) {
    Algorithm solver;
    long time, delta;
    int count = 1;
    long allPositionsTime = 0;

    for (Chessboard position : positions) {
      solver = new AStarAlgorithm<>(
          actionsGenerator,
          heuristicCalculator);
//      if (position.getMovesLeft() > 5) continue;

      outputStream.println(count + "; " + position.getFEN());
      time = System.currentTimeMillis();
      List<ChessNode> solution = solver.solve(position);
      delta = System.currentTimeMillis() - time;
      outputStream.println(delta);
      String output = solution.stream()
          .map(asNode -> asNode.getAction().get().toString())
          .collect(Collectors.joining(";"));
      outputStream.println(output);
      outputStream.println();
      outputStream.flush();
      allPositionsTime += delta;
      count += 1;
      Cache.clear();
    }

    outputStream.println("Solved in: " + allPositionsTime);
  }

  private static void solvePosition(Chessboard position,
                                    ActionsGenerator actionsGenerator,
                                    ChessHeuristicCalculator heuristicCalculator) {
    Algorithm solver = new AStarAlgorithm<>(
        actionsGenerator,
        heuristicCalculator);
    List<ChessNode> solution = solver.solve(position);
    String output = solution.stream()
        .map(asNode -> asNode.getAction().get().toString())
        .collect(Collectors.joining(";"));
    System.out.print(output);
  }

  private static String lastCommitSummary() {
    try {
      Runtime rt = Runtime.getRuntime();
      Process process = rt.exec("git log -1");
      BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
      String s;
      StringBuilder builder = new StringBuilder();
      while ((s = reader.readLine()) != null) {
        builder.append(s);
        builder.append("\n");
      }
      builder.append("\n---------\n\n");
      return builder.toString();
    } catch (IOException e) {
      return "";
    }
  }
}
