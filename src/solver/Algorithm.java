package solver;

import solver.node.Node;

import java.util.List;

public interface Algorithm<A, S, N extends Node<A, S>> {
  List<N> solve(S initialState);
}
