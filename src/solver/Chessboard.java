package solver;

import Rules.Move;

import java.util.Arrays;
import java.util.Optional;

public class Chessboard extends Rules.Chessboard {
  private Optional<Position> opponentsKing = Optional.empty();

  protected Chessboard(int[][] board, int gameStatus, int color, int movesLeft) {
    super(board, gameStatus, color, movesLeft);
  }

  public static Chessboard fromFEN(String fen) {
    Rules.Chessboard chessboard = Rules.Chessboard.getChessboardFromFEN(fen);
    return new Chessboard(
        chessboard.getBoard(),
        chessboard.getGameStatus(),
        chessboard.getColor(),
        chessboard.getMovesLeft());
  }

  /////

  public Position positionOfOpponentsKing() {
    if (opponentsKing.isPresent()) {
      return opponentsKing.get();
    }

    Position position;
    switch (getColor()) {
      case Rules.Chessboard.WHITE:
        position = positionOfBlackKing();
        break;
      case Rules.Chessboard.BLACK:
        position = positionOfWhiteKing();
        break;
      default:
        position = null;
    }
    opponentsKing = Optional.of(position);
    return position;
  }

  public int pieceAt(Move move) {
    int[] coordinates = move.getCoordinates();
    int piece = board[coordinates[2]][coordinates[3]];
    return piece;
  }

  public Position positionOfWhiteKing() {
    return positionOf(Rules.Chessboard.KING);
  }

  public Position positionOfBlackKing() {
    return positionOf(Rules.Chessboard.KING_B);
  }

  public Position positionOf(int piece) {
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {
        if (board[i][j] == piece)
          return new Position(i, j);
      }
    }
    return null;
  }

  public boolean isCheckmate() {
    return gameStatus == 2;
  }

  @Override
  public void makeMove(Move move) {
    makeMove(move, false);
  }

  @Override
  public Chessboard copy() {
    int[][] nv = new int[this.board.length][this.board[0].length];

    for(int i = 0; i < nv.length; ++i) {
      nv[i] = Arrays.copyOf(this.board[i], this.board[i].length);
    }

    Chessboard b = new Chessboard(nv, this.gameStatus, this.color, this.movesLeft);
    b.a1 = this.a1;
    b.a8 = this.a8;
    b.e1 = this.e1;
    b.e8 = this.e8;
    b.h1 = this.h1;
    b.h8 = this.h8;
    b.movesCalculated = false;
    return b;
  }
}