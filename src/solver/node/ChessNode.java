package solver.node;

import Rules.Move;
import solver.Chessboard;
import solver.caching.ZobristHasher;

import java.util.Optional;

public class ChessNode extends Node<Move, Chessboard> {
  public final int depth;
  private Optional<Long> cachedHash = Optional.empty();

  public ChessNode(Chessboard newState, Move action, ChessNode parent) {
    super(newState, action, parent);
    this.depth = parent.depth + 1;
  }

  public ChessNode(Chessboard state) {
    super(state);
    depth = 0;
  }

  @Override
  public boolean isGoalReached() {
    return state.isCheckmate();
  }

  @Override
  public Long hashString() {
    if (cachedHash.isPresent()) {
      return cachedHash.get();
    }
    int[][] board = getState().getBoard();
    long hash = ZobristHasher.singleton.hashPosition(board, getState().getMovesLeft());
    cachedHash = Optional.of(hash);
    return hash;
  }
}

//  private int compare(Move a, Move b) {
//    int[] c1 = a.getCoordinates();
//    int[] c2 = b.getCoordinates();
//    for (int i = 0; i < 4; i++) {
//      if (c1[i] != c2[i]) return c1[i] - c2[i];
//    }
//    return 0;
//  }

//  @Override
//  public int hashCode() {
//    return fenHashCode();
////    return chessBoardHashCode();
//  }
//
//  private int fenHashCode() {
//    return state.getFEN().hashCode();
//  }
//
//  private int chessBoardHashCode() {
//    return java.util.Arrays.deepHashCode(state.getBoard());
//  }
//
//  @Override
//  public boolean equals(Object obj) {
//    return hashCode() == obj.hashCode();
//  }
//}
