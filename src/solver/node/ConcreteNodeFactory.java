package solver.node;

import solver.Chessboard;
import Rules.Move;

public class ConcreteNodeFactory implements NodeFactory<Move, Chessboard, ChessNode> {
  @Override
    public ChessNode node(Move withAction, ChessNode parent) {
    Chessboard newState = parent.getState().copy(); // TODO: - Optimize this!!!!
    newState.makeMove(withAction);
    ChessNode node = new ChessNode(newState, withAction, parent);
    return node;
  }

  @Override
  public ChessNode node(Chessboard initialState) {
    ChessNode node = new ChessNode(initialState);
    return node;
  }
}
