package solver.node;

import solver.caching.Hashable;

import java.util.Optional;

public abstract class Node<A, S> implements Hashable<Long> {
  protected double g = Double.MAX_VALUE;
  protected double f = Double.MAX_VALUE;
  protected S state;
  protected Optional<A> action;
  protected Optional<Node> parent;

  public Node(S newState, A action, Node<A, S> parent) {
    this.state = newState;
    this.action = Optional.of(action);
    this.parent = Optional.of(parent);
  }

  public Node(S state) {
    this.state = state;
    this.action = Optional.empty();
    this.parent= Optional.empty();
  }

  public abstract boolean isGoalReached();

  public void setG(double g) {
    this.g = g;
  }

  public void setF(double f) {
    this.f = f;
  }

  public double getG() {
    return g;
  }

  public double getF() {
    return f;
  }

  public double getScore() {
    return g + f;
  }

  public S getState() {
    return state;
  }

  public Optional<A> getAction() {
    return action;
  }

  public Optional<Node> getParent() {
    return parent;
  }
}
