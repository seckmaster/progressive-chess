package solver.node;

public interface NodeFactory<A, S, N> {
  N node(A withAction, N parent);
  N node(S fromState);
}
