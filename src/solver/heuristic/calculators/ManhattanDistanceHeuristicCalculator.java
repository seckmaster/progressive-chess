package solver.heuristic.calculators;

import Rules.Move;
import solver.Chessboard;
import solver.Position;
import solver.heuristic.HeuristicCalculator;
import solver.node.ChessNode;

/**
 * Sum of manhattan distances between all pieces and opponent's king.
 */
public class ManhattanDistanceHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
  @Override
  public double heuristic(ChessNode node) {
    switch (node.getState().getColor()) {
      case Rules.Chessboard.WHITE:
        return sumDistancesFromWhitePiecesToBlackKing(node.getState());
      case Rules.Chessboard.BLACK:
        return sumDistancesFromBlackPiecesToWhiteKing(node.getState());
      default:
        return -1;
    }
  }

  private int sumDistancesFromWhitePiecesToBlackKing(Chessboard state) {
    return sumDistances(state, true);
  }

  private int sumDistancesFromBlackPiecesToWhiteKing(Chessboard state) {
    return sumDistances(state, false);
  }

  private int sumDistances(Chessboard state, boolean white) {
    int distances = 0;
    Position king = state.positionOfOpponentsKing();
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {
        if (white) {
          if (state.getBoard()[i][j] > Rules.Chessboard.EMPTY)
            distances += manhattanDistance(king.x, king.y, i, j);
        } else if (state.getBoard()[i][j] < Rules.Chessboard.EMPTY) {
          distances += manhattanDistance(king.x, king.y, i, j);
        }
      }
    }
    return distances;
  }

  private int manhattanDistance(int x1, int y1, int x2, int y2) {
    return Math.abs(x1 - x2) + Math.abs(y1 - y2);
  }
}
