package solver.heuristic.calculators;

import Rules.Move;
import solver.Chessboard;
import solver.Position;
import solver.heuristic.HeuristicCalculator;
import solver.node.ChessNode;

/**
 * How many available square does the opponent's king have.
 */
public class KingMobilityHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
  @Override
  public double heuristic(ChessNode node) {
    Position opponentsKing = node.getState().positionOfOpponentsKing();
    return 7 - numberOfOccupiedMatingSquares(node.getState(), opponentsKing);
  }

  private int numberOfOccupiedMatingSquares(Chessboard chessboard, Position kingPosition) {
    int count = 0;
    for (int i = kingPosition.x - 1; i < kingPosition.x + 2; i++) {
      for (int j = kingPosition.y - 1; j < kingPosition.y + 2; j++) {
        if (i < 0 || i >= 8 || j < 0 || j >= 8) continue;
        if (chessboard.getBoard()[i][j] == Rules.Chessboard.EMPTY) ++count;
      }
    }
    return count;
  }
}
