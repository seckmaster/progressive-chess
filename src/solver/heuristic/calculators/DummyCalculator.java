package solver.heuristic.calculators;

import Rules.Move;
import solver.Chessboard;
import solver.heuristic.HeuristicCalculator;
import solver.node.ChessNode;

public class DummyCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
  @Override
  public double heuristic(ChessNode node) {
    return 0;
  }
}
