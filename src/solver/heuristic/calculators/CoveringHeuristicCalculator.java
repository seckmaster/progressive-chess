package solver.heuristic.calculators;

import Rules.Move;
import solver.Chessboard;
import solver.Position;
import solver.heuristic.HeuristicCalculator;
import solver.node.ChessNode;
import solver.caching.Cache;

/**
 * Number of covered mating squares.
 * More specifically, number of possible moves which are <= 2 manhattan distance away
 * from the opponent's king.
 */
public class CoveringHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
  @Override
  public double heuristic(ChessNode node) {
    double covering = calculateCovering(
        Cache.movesforNode(node),
        node.getState().positionOfOpponentsKing());
    return covering;
  }

  private double calculateCovering(Iterable<Move> availableMoves, Position opponentsKing) {
    int covering = 0;
    for (Move move: availableMoves) {
      if (isMoveCovering(move, opponentsKing)) {
        covering += 1;
      }
    }
    return covering;
  }

  private boolean isMoveCovering(Move move, Position king) {
    int[] coordinates = move.getCoordinates();
    int distance = manhattanDistance(coordinates[2], coordinates[3], king.x, king.y);
    return distance <= 2;
  }

  private int manhattanDistance(int x1, int y1, int x2, int y2) {
    return Math.abs(x1 - x2) + Math.abs(y1 - y2);
  }
}
