package solver.heuristic;

import solver.node.Node;

public interface HeuristicCalculator<A, S, N extends Node<A, S>> {
  double heuristic(N node);
}
