package solver.heuristic.pieces;

import Rules.Move;
import solver.Chessboard;
import solver.heuristic.HeuristicCalculator;
import solver.node.ChessNode;

public class KingHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
  private HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator;
  private HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator;
  private HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator;

  public KingHeuristicCalculator(HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator,
                                 HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator,
                                 HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator) {
    this.kingMobilityCalculator = kingMobilityCalculator;
    this.manhattanDistanceCalculator = manhattanDistanceCalculator;
    this.coveringCalculator = coveringCalculator;
  }

  @Override
  public double heuristic(ChessNode node) {
//    double mobility = kingMobilityCalculator.heuristic(state, action);
//    double manhattan = manhattanDistanceCalculator.heuristic(state, action);
//    return mobility + manhattan;
    return Double.MAX_VALUE; // TODO: -
  }
}
