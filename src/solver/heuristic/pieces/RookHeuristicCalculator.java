package solver.heuristic.pieces;

import Rules.Move;
import solver.Chessboard;
import solver.heuristic.HeuristicCalculator;
import solver.node.ChessNode;

public class RookHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
  private HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator;
  private HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator;
  private HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator;

  public RookHeuristicCalculator(HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator,
                                 HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator,
                                 HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator) {
    this.kingMobilityCalculator = kingMobilityCalculator;
    this.manhattanDistanceCalculator = manhattanDistanceCalculator;
    this.coveringCalculator = coveringCalculator;
  }

  @Override
  public double heuristic(ChessNode node) {
    double mobility = kingMobilityCalculator.heuristic(node);
    double manhattan = manhattanDistanceCalculator.heuristic(node);
    double covering = coveringCalculator.heuristic(node);
    return mobility + manhattan + covering;
  }
}
