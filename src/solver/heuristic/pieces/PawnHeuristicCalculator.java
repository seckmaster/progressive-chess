package solver.heuristic.pieces;

import Rules.Move;
import solver.Chessboard;
import solver.heuristic.HeuristicCalculator;
import solver.node.ChessNode;

public class PawnHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
  private HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator;
  private HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator;
  private HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator;

  public PawnHeuristicCalculator(HeuristicCalculator<Move, Chessboard, ChessNode> kingMobilityCalculator,
                                 HeuristicCalculator<Move, Chessboard, ChessNode> manhattanDistanceCalculator,
                                 HeuristicCalculator<Move, Chessboard, ChessNode> coveringCalculator) {
    this.kingMobilityCalculator = kingMobilityCalculator;
    this.manhattanDistanceCalculator = manhattanDistanceCalculator;
    this.coveringCalculator = coveringCalculator;
  }

  @Override
  public double heuristic(ChessNode node) {
    double promotion = distanceToPromotion(node.getState(), node.getAction().get());
    double reward = rewardForPromotion(node.getAction().get());
    double mobility = kingMobilityCalculator.heuristic(node);
    double manhattan = manhattanDistanceCalculator.heuristic(node);
    return promotion + reward + manhattan + mobility;
  }

  public int distanceToPromotion(Chessboard state, Move action) {
    switch (state.getColor()) {
      case Rules.Chessboard.WHITE:
        return 7 - action.getCoordinates()[2];
      case Rules.Chessboard.BLACK:
        return action.getCoordinates()[2];
      default:
        return -1;
    }
  }

  public double rewardForPromotion(Move action) {
    if (action.getPromotion() == Rules.Chessboard.EMPTY) return 0;
    int piece = Math.abs(action.getPromotion());
    switch (piece) {
      case Rules.Chessboard.QUEEN:
        return 0;
      case Rules.Chessboard.ROOK:
        return 1;
      case Rules.Chessboard.KNIGHT:
      case Rules.Chessboard.BISHOP:
        return 3;
      default:
        return 5;
    }
  }
}