package solver.heuristic;

import solver.Chessboard;
import Rules.Move;
import solver.node.ChessNode;

import java.util.Map;

public class ChessHeuristicCalculator implements HeuristicCalculator<Move, Chessboard, ChessNode> {
  private int solutionSize;
  private Map<Integer, HeuristicCalculator> calculators; // TODO: - maybe convert to array
  private HeuristicCalculator<Move, Chessboard, ChessNode> manhattanCalculator;

  public ChessHeuristicCalculator(int solutionSize,
                                  Map<Integer, HeuristicCalculator> calculatorMap,
                                  HeuristicCalculator<Move, Chessboard, ChessNode> manhattanCalculator) {
    this.solutionSize = solutionSize;
    this.calculators = calculatorMap;
    this.manhattanCalculator = manhattanCalculator;
  }

  public void registerCalculatorForPiece(HeuristicCalculator calculator, int piece) {
    calculators.put(Math.abs(piece), calculator);
  }

  @Override
  public double heuristic(ChessNode node) {
    if (!node.getAction().isPresent()) {
      // heuristic for the initial state
      // TODO: - figure out which heuristic, or a group of them, suits best
      return manhattanCalculator.heuristic(node);
    }
    int piece = node.getState().pieceAt(node.getAction().get());
    return heuristicForPiece(piece, node);
  }

  private double heuristicForPiece(int piece, ChessNode node) {
    HeuristicCalculator calculator = calculators.get(Math.abs(piece));
    if (calculator == null) {
      return manhattanCalculator.heuristic(node);
    }
    return calculator.heuristic(node);
  }
}
