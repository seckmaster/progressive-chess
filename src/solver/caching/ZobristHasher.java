package solver.caching;

import Rules.Chessboard;

import java.util.Random;

public class ZobristHasher {
  private long[][][] zobristTable = new long[8][8][12];
  private long[] movesLeftTable = new long[10];

  public ZobristHasher() {
    Random random = new Random();
    for (int i = 0; i<8; i++) {
      for (int j = 0; j < 8; j++) {
        for (int k = 0; k < 12; k++) {
          zobristTable[i][j][k] = Math.abs(random.nextLong());
        }
      }
    }
    for (int i = 0; i < movesLeftTable.length; i++) {
      movesLeftTable[i] = Math.abs(random.nextLong());
    }
  }

  public long hashPosition(int[][] board, int movesLeft) {
    long h = 0;
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {
        if (board[i][j] == Chessboard.EMPTY) { continue; }
        int piece = board[i][j];
        if (piece < Chessboard.EMPTY)
          piece = 12 + piece;
        else
          piece -= 1;
        h ^= zobristTable[i][j][piece];
      }
    }
    h ^= movesLeftTable[movesLeft];
    return h;
  }

  public static ZobristHasher singleton = new ZobristHasher(); // TODO: -
}
