package solver.caching;

public interface Hashable<Result> {
  Result hashString();
}
