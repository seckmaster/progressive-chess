package solver.caching;

import Rules.Move;
import solver.node.ChessNode;

import java.util.HashMap;

public class Cache {
  private static HashMap<Long, Iterable<Move>> movesCache = new HashMap<>();

  public static Iterable movesforNode(ChessNode node) {
    Long hash = node.hashString();
    Iterable cachedMoves = movesCache.get(hash);
    if (cachedMoves == null) {
      Iterable moves = node.getState().getMoves();
      movesCache.put(hash, moves);
      return moves;
    }
    return cachedMoves;
  }

  public static void clear() {
    movesCache.clear();
  }
}
