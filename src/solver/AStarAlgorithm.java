package solver;

import solver.actions.ActionsGenerator;
import solver.heuristic.HeuristicCalculator;
import solver.node.ChessNode;
import solver.node.Node;

import java.util.*;

public class AStarAlgorithm<A, S, N extends Node<A, S>> implements Algorithm<A, S, N> {
  // Provider of actions for each state
  private ActionsGenerator<A, S, N> actionsGenerator;
  // Heuristics provider
  private HeuristicCalculator<A, S, N> heuristicCalculator;
  // The set of nodes already evaluated
  private Map<Long, N> closed = new HashMap<>();
  // The set of currently discovered nodes that are not evaluated yet.
  // Initially, only the start node is known.
  private Map<Long, N> open = new HashMap<>();
  private Queue<N> queue;

  private long startTime = System.currentTimeMillis();

  public AStarAlgorithm(ActionsGenerator<A, S, N> actionsGenerator, HeuristicCalculator<A, S, N> heuristicCalculator) {
    this.actionsGenerator = actionsGenerator;
    this.heuristicCalculator = heuristicCalculator;
    this.queue = new PriorityQueue<>(Comparator.comparingDouble(Node::getF));
  }

  @Override
  public List<N> solve(S initialState) {
    N startingNode = actionsGenerator.nodeFactory().node(initialState);
    // The cost of going from start to start is zero.
    startingNode.setG(0);

    // For the first node, that value is completely heuristic.
    startingNode.setF(heuristicCalculator.heuristic(startingNode));

    // The set of currently discovered nodes that are not evaluated yet.
    // Initially, only the start node is known.
    queue.add(startingNode);
    open.put(startingNode.hashString(), startingNode);

    while (!queue.isEmpty()) {
      if (System.currentTimeMillis() - startTime >= 60000) { break; }

      N current = promisingNode();
      open.remove(current.hashString());
      closed.put(current.hashString(), startingNode);
      if (current.isGoalReached()) {
        // done searching
        return reconstructPath(current);
      }

      Iterable<A> actions = actionsGenerator.possibleActionsForState(current);
      for (A action: actions) {
        if (System.currentTimeMillis() - startTime >= 60000) { break; }

        N neighbour = actionsGenerator.nodeFactory().node(action, current);
        if (closed.containsKey(neighbour.hashString())) {
          // Ignore the neighbor which is already evaluated.
          continue;
        }
        // The distance from start to a neighbor
        double tentativeScore = current.getG() + 1;

        N found = open.get(neighbour.hashString());
        if (found == null) {
          updateNode(neighbour, tentativeScore);
          open.put(neighbour.hashString(), neighbour);
          queue.add(neighbour);
        } else {
          if (tentativeScore >= neighbour.getG()) {
            continue;
          } else {
            updateNode(neighbour, tentativeScore);
          }
        }
      }
    }
    return Collections.emptyList();
  }

  private void updateNode(N node, double score) {
    node.setG(score);
    node.setF(node.getG() + heuristicCalculator.heuristic(node));
  }

  private N promisingNode() {
    return queue.poll();
  }

  private List<N> reconstructPath(N lastNode) {
    LinkedList<N> path = new LinkedList<>();
    path.add(lastNode);
    Optional parent = lastNode.getParent();
    while (parent.isPresent()) {
      N node = (N) parent.get();
      if (!node.getAction().isPresent()) { break; }
      path.add(0, node);
      parent = node.getParent();
    }
    return path;
  }
}
