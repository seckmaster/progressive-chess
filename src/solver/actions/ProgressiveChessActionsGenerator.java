package solver.actions;

import Rules.Move;
import solver.Chessboard;
import solver.caching.Cache;
import solver.node.ChessNode;
import solver.node.ConcreteNodeFactory;
import solver.node.NodeFactory;

public class ProgressiveChessActionsGenerator implements ActionsGenerator<Move, Chessboard, ChessNode> {
  private NodeFactory factory = new ConcreteNodeFactory();

  @Override
  public Iterable possibleActionsForState(ChessNode node) {
    return Cache.movesforNode(node);
  }

  @Override
  public NodeFactory<Move, Chessboard, ChessNode> nodeFactory() {
    return factory;
  }
}
