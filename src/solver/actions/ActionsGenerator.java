package solver.actions;

import solver.node.Node;
import solver.node.NodeFactory;

/***
 *
 * @param <S> state
 * @param <A> action
 */
public interface ActionsGenerator<A, S, N extends Node<A, S>> {
  Iterable<A> possibleActionsForState(N node);
  NodeFactory<A, S, N> nodeFactory();
}
