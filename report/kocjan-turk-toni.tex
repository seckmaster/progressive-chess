% To je predloga za poročila o domačih nalogah pri predmetih, katerih
% nosilec je Blaž Zupan. Seveda lahko tudi dodaš kakšen nov, zanimiv
% in uporaben element, ki ga v tej predlogi (še) ni. Več o LaTeX-u izveš na
% spletu, na primer na http://tobi.oetiker.ch/lshort/lshort.pdf.
%
% To predlogo lahko spremeniš v PDF dokument s pomočjo programa
% pdflatex, ki je del standardne instalacije LaTeX programov.

\documentclass[a4paper,11pt]{article}
\usepackage{a4wide}
\usepackage{fullpage}
\usepackage[utf8x]{inputenc}
\usepackage[slovene]{babel}
\selectlanguage{slovene}
\usepackage[toc,page]{appendix}
\usepackage[pdftex]{graphicx} % za slike
\usepackage{setspace}
\usepackage{color}
\definecolor{light-gray}{gray}{0.95}
\usepackage{listings} % za vključevanje kode
\usepackage{hyperref}
\usepackage{titlesec}
\usepackage{listings}
\usepackage{hyperref}


% Python style for highlighting
\definecolor{keywords}{RGB}{30,30,255}
\definecolor{comments}{RGB}{0,0,113}
\definecolor{red}{RGB}{160,0,0}
\definecolor{green}{RGB}{50,50,50}

\lstset{language=Java, 
	basicstyle=\ttfamily\small, 
	keywordstyle=\color{keywords},
	commentstyle=\color{comments},
	stringstyle=\color{red},
	showstringspaces=false,
	identifierstyle=\color{green}}
\renewcommand{\baselinestretch}{1.2} % za boljšo berljivost večji razmak
\renewcommand{\appendixpagename}{\normalfont\Large\bfseries{Priloge}}


\titleformat{name=\section}[runin]
  {\normalfont\bfseries}{}{0em}{}
\titleformat{name=\subsection}[runin]
  {\normalfont\bfseries}{}{0em}{}


% header
\makeatletter
\def\@maketitle{%
  \noindent
  \begin{minipage}{2in}
  \@author
  \end{minipage}
  \hfill
  \begin{minipage}{1.2in}
  \textbf{\@title}
  \end{minipage}
  \hfill
  \begin{minipage}{1.2in}
  \@date
  \end{minipage}
  \par
  \vskip 1.5em}
\makeatother


\lstset{ % nastavitve za izpis kode, sem lahko tudi kaj dodaš/spremeniš
language=Python,
basicstyle=\footnotesize,
basicstyle=\ttfamily\footnotesize\setstretch{1},
backgroundcolor=\color{light-gray},
}


% Naloga
\title{Progresivni šah}
% Ime Priimek (vpisna)
\author{Toni Kocjan Turk (63140110)}
\date{\today}

\begin{document}

\maketitle

\subsection{Uvod.}

Progresivni šah je različica šaha, kjer igralcema število možnih potez z vsako potezo progresivno narašča. Beli začne z eno, nato črni nadaljuje z dvema, itd. Cilj te domače naloge je napisati program, ki za podano stanje igre, poišče zaporedje legalnih potez, ki vodijo do šah-mata, v čim krajšem času. Pri implementaciji rešitve si pomagamo z algoritmom {\ttfamily A*}. Za rešitev smo lahko izbirali med {\ttfamily Javo} in {\ttfamily Pythonom}, mi smo se odločili za {\ttfamily Javo}.

\subsection{Definicija problema.}

Vhodno stanje šahovnice je začetno stanje preiskovanja $ s_0 $. Iz vsakega stanja $s_i$ lahko preidemo v stanje $s_{i+1}$ preko akcije $a_i$. Akcija je v našem primeru \textbf{legalen} premik figure na šahovnici. Za vsako stanje lahko obstaja poljubno mnogo akcij ${a_0, a_1, ..., a_n}$. Z iskanjem zaključimo, ko pridemo v stanje, za katerega {\ttfamily goal} funkcija vrne {\ttfamily true}. Z drugimi besedami, ko najdemo šah-mat.

\subsection{Implementacija algoritma.}

Algoritem {\ttfamily A*} je sestavljen iz treh glavnih komponent:

\begin{itemize}
	\item generiranja možnih akcij za neko stanje $s_i$
	\item evalvacije cene prehoda iz stanja $s_i$ v stanje $s_{i+1}$ preko akcije $a_i$ in
	\item jedra algoritma
\end{itemize}

Poudarek je na generični implementaciji, kar pomeni, da bi lahko z le malo truda implementacijo uporabili tudi za reševanje drugih, podobnih problemov. 

S tem namenom predstavimo dva vmesnika, preko katerih glavni komponenti uporabimo v jedru algoritma: \newline

\textbf{Generiranje akcij:}

\begin{lstlisting}[language=Java]
public interface ActionsGenerator<A, S, N extends Node<A, S>> {
  Iterable<A> possibleActionsForState(N node);
  NodeFactory<A, S, N> nodeFactory();
}
\end{lstlisting}

Pri implementaciji vmesnika si pomagamo s knjižnico {\ttfamily ProgressiveChess}, ki za dano stanje na šahovnici zgenerira seznam vseh legalnih potez:

\begin{lstlisting}[language=Java]
...
@Override
public Iterable<Move> possibleActionsForState(ChessNode node) {
  // `node.getState()` vrne objekt tipa `Chessboard`
  return node.getState().getMoves();
}
...
\end{lstlisting}

\textbf{Evalvacija hevristik:}

\begin{lstlisting}[language=Java]
public interface HeuristicCalculator<A, S, N extends Node<A, S>> {
  double heuristic(N node);
}
\end{lstlisting}

Kot začetno hevristiko smo implementirali t.i {\ttfamily manhattan distance} hevristiko, ki sešteje razdaljo od vseh igralčevih figur do nasprotnikovega kralja. Ker smo si že v samem začetku želeli pripraviti čimbolj enostaven mehanizem prilagajanja hevristik glede na tip figure, dodamo še šest dodatnih  implementacij vmesnika, enega za vsako figuro. Ideja je bila, da bo za hitro reševanje potrebno mikro-prilagajanje posameznih hevristik, ločeno glede na figuro. Za enostaven pregled in kontroliranje hevristik uvedemo še dodatno implementacijo. Ta skrbi, da se glede na akcijo izračuna hevristika na podlagi premaknjene figure. \newline

\textbf{Hashing:}

Jedro algoritma {\ttfamily A*} je bazirano na dveh podatkovnih strukturah, v kateri vstavljamo vozlišča. Bolj natančno gre za {\ttfamily HashMap} in {\ttfamily PriorityQueue}. Za učinkovito vstavljanje objektov v razpršeno tabelo potrebujemo {\ttfamily hash} funkcijo, ki nam za dan objekt (v našem primeru govorimo o stanju na šahovnici) vrne neko celo število (t.i. hash vrednost). Pogosto pravimo, da je prezgodnja optimizacija glavni razlog za naše težave, zato se v tej fazi še ne ukvarjamo z učinkovitostjo hash-inga, ampak želimo najprej pozicije samo nekako shraniti v slovar. Za začetek uporabimo kar {\ttfamily FEN} notacijo, ki je seveda unikatna za vsako stanje šahovnice.

\subsection{Prvi krog optimizacij (pred prvotno oddajo).}

Za odkrivanje ozkih grl izvajanja algoritma si pomagamo s programom \textbf{JProfiler}. Gre za programsko opremo, ki omogoča \textit{profiling} programov, ki tečejo v JVM-ju. Pri sami optimizaciji nam je orodje prišlo izjemno prav. Spodnja slika prikazuje (ne v celoti) odčitek izvajanja programa pred optimizacijami:

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[scale=0.27]{jprofile1.png}
		\caption{Odčitek učinkovitosti izvajanja programa.}
		\label{slika1}
	\end{center}
\end{figure}

Zelo jasno razvidne so tri najbolj potratne operacije: `kopiranje` šahovnice iz {\ttfamily FEN} notacije, hashing šahovnic s pomočjo {\ttfamily FEN} notacije ter izračun hevristik. 

Kopiranje šahovnice pohitrimo tako, da namesto kreiranja nove šahovnice iz obstoječe preko FEN notacije, uporabimo metodo {\ttfamily copy}, ki je že del prej omenjene knjižnice. 

Razlog za potratno računanje hevristik se je skrival v tem, da so se pri računanju nenehno generirale poteze za dano stanje, kar je zelo potratna operacija. Problem enostavno rešimo tako, da za vsako pozicijo poteze zgeneriramo samo enkrat, jih shranimo v cache, in nato vsakič naslednjič vzamemo iz njega.

Optimizacija same hash funkcije je bila nekoliko težji problem. Po raziskovanju problema na spletu, predvsem na portalu \href{http://chessprogramming.org/}{Chess programming}, najdemo algoritem, ki rešuje naš problem. In sicer gre za {\ttfamily Zobrist hashing} algoritem, ki glede na dano šahovnico izračuna neko veliko številko, s ciljem, da je le-ta čimbolj unikatna (da je čim manj različnih stanj šahovnice, ki privedejo do iste vrednosti). Algoritem je sila preprost: zgeneriramo 8 x 8 x 12 naključnih števil (za vsako figuro eno) ter nato izračunamo xor vseh teh naključnih števil glede na figuro v nekem polju. 

Pomembna obzervacija pri implementaciji je, da originalen algoritem ne vzame v račun tudi števila preostalih potez igralca. Zlahka si lahko zamislimo situacijo, kjer smo od cilja oddaljeni dve potezi, a smo v to stanje prišli z eno preostalo potezo in bi lahko stanje pomotoma označili kot nerešljivo. Zato je pomembno, da prilagodimo algoritem, da bo upošteval tudi to bistveno informacijo. Za ta namen zgeneriramo dodatnih 10 naključnih števil (dovolj bi bilo 9, saj je to najdaljša rešitev v množici danih problemov), in dodatno izračunamo xor glede na preostalo število potez.

Kot zanimivost povemo, da postane po naštetih optimizacijah najbolj potratna operacija generiranje veljavnih potez (porabi kar cca. 60\% celotnega časa izvajanja):

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[scale=0.6]{jprofile2.png}
		\caption{Učinkovitost izvajanja po optimizacijah.}
		\label{slika1}
	\end{center}
\end{figure}

\subsection{Zadnjih pet.}

V tej fazi oddamo trenutno implementacijo na strežnik v test. Rezultat je bil izjemno spodbuden, saj smo rešili kar 55 problemov, in to v solidnem času 242.47 sekund (takrat je bilo to dovolj za prvo mesto). 

Za dosego končnega cilja (rešitev vseh 60 problemov) so bile spremembe in prilagajanja zelo majhna, a so lahko kljub temu zelo majhne spremembe drastično vplivale na čas iskanja nekaterih daljših rešitev. Izkazalo se je, da k pohitritvi nekoliko pripomore dodatna hevristika za kmete. Tiste kmete, ki so bližje promociji ali pa so promovirani, je dobro nagraditi. Dodatno za kralja gledamo oddaljenost od nasprotnikovega kralja, če je ta previsoka vrnemo neko zelo veliko vrednost, sicer njegovo razdaljo. Približna ideja tega je, da se v pozicijah, ko je kralj predaleč stran od nasprotnikovega, ne spustimo v njegove poteze in ne izgubljamo časa po nepotrebnem. Še ena enostavna hevristika, ki jo uporabimo pri kmetih je t.i. \textit{king mobility} hevristika, ki prešteje prazna polja okoli nasprotnikovega kralja, in jo uporabimo samo za kmete. Poleg naštetih sprememb pripomore tudi sprememba v računanju {\ttfamily g} vrednosti (razdalje med vozlišči) iz konstatno 1 na $g = length - depth$ kjer je $length$ dolžina rešitve in $depth$ globina trenutnega vozlišča.

Z naštetimi spremembami izboljšamo rezultat na 58 in čas zmanjšamo na 189.59 sekund. 

Za zadnji dve rešitvi uporabimo metodo \textit{naključnega iskanja}: definiramo štiri parametre, ki jih uporabimo kot uteži pri hevristikah za kmeta in kralja. Tem utežem nato naključno določimo vrednosti znotraj smiselne zaloge vrednosti, rešimo vseh 60 stanj, ter shranimo končen čas. To ponavljamo nekaj časa (nekaj ur) ter izberemo kombinacijo, ki je privedla do najboljše rešitve. Najboljši rezultat je rešenih vseh 60 primerov v 151 sekundah.

\subsection{Meritve in primerjava rezultatov.}

Za uspešno strategijo optimizacije so ključne meritve, brez njih namreč težko primerjamo rešitve in razmišljamo o tem, katera je boljša. Za ta namen pripravimo enostaven mehanizem za logiranje porabljenega časa: ob vsakem zagonu programa se ustvari datoteka kamor se shranjujejo porabljeni časi za posamezne pozicije. Poleg tega se notri shrani tudi skupen porabljen čas ter povzetek zadnjega git commit-a (timestamp, message, ...), da nam je lažje identificirati posamezne log-e.

Sledijo trije grafi, ki prikazujejo napredek naše rešitve, predvsem na ključnih točkah, ko pride do večjih pohitritev (\textit{opomba:} čas reševanja posameznih pozicij je omejen na 40 sekund).

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[scale=0.7]{plot4.png}
		\caption{Meritve časov izvajanja.}
		\label{meritve1}
	\end{center}
\end{figure}

Poudariti je potrebno še, da so meritve \ref{meritve1} izvedene na našem računalniku (Intel Core i7 2.8 GHz, 16GB RAM), zato jih ni smiselno relativno primerjati z rezultati na strežniku. Po naših estimacijah na podlagi razlike med najboljšim časom na strežniku in najboljšim časom lokalnega testiranja, je strežnik porabil cca. 4x več časa. Naš računalnik za rešitev vseh problemov potrebuje približno 31 sekund.

\newpage
\newpage
\section{Izjava o izdelavi domače naloge.}
Domačo nalogo in pripadajoče programe sem izdelal sam.
\href{https://gitlab.com/seckmaster/progressive-chess}{Repozitorij}

\end{document}
